//Components/Search

import React from 'react';
import { 
    View, 
    TextInput, 
    Button, 
    StyleSheet,
    FlatList,
    ActivityIndicator
} from 'react-native';

import FilmItem from './FilmItem'

import { getFilmFromApiWithSearchText } from '../API/TMDBApi'

export default class Search extends React.Component {

    constructor(props) {
        super(props)

        // Variable
        this.searchedText = ""
        this.page = 0
        this.totalPage = 0

        // State
        this.state = { 
            films: [],
            isLoading: false 
        }
    }

    _loadFilms() {
        getFilmFromApiWithSearchText(this.searchedText, this.page+1).then(data => {
            if(this.searchedText.length > 0) {
                this.setState({isLoading: true})
                getFilmFromApiWithSearchText(this.searchedText).then(data => {
                    this.page = data.page
                    this.totalPage = data.total_pages
                    this.setState({
                        films: [...this.state.films, ...data.results],
                        isLoading: false
                    })
                    console.log("Page : " + this.page + " / TotalPages : " + this.totalPages + " / Nombre de films : " + this.state.films.length)
                })
            }
        });
    }

    _searchTextInputChanged(text) {
        this.searchedText = text
    }

    _displayLoading() {
        if(this.setState.isLoading){
            <View style={styles.loading_container}>
                <ActivityIndicator size='large' />
            </View>
        }
    }

    _searchFilms() {
        this.page = 0
        this.totalPages = 0
        this.setState({
            films: []
        })
        // J'utilise la paramètre length sur mon tableau de films pour vérifier qu'il y a bien 0 film
        console.log("Page : " + this.page + " / TotalPages : " + this.totalPages + " / Nombre de films : " + this.state.films.length)
        this._loadFilms()
    }

    _displayDetailForFilm = (idFilm) => {
        console.log("Display film with id " + idFilm)
        this.props.navigation.navigate("FilmDetail", {idFilm: idFilm})
    }

    render() {
        console.log(this.props)
        return (
            <View style={styles.view}>
                <TextInput placeholder="Titre du Film" style={styles.textinput} 
                onChangeText={(text) => this._searchTextInputChanged(text)} 
                onSubmitEditing={() => this._searchFilms()}/>
                <Button title="Rechercher" onPress={() => this._searchFilms()} />
                <FlatList
                    data={this.state.films}
                    keyExtractor={(item) => item.id.toString()}
                    renderItem={({item}) => <FilmItem film={item} displayDetailForFilm={this._displayDetailForFilm}/>}
                    onEndReachedThreshold={0.5}
                    onEndReached={function(){
                        if(this.page < this.totalPage) {
                            this._loadFilms()
                        }
                    }}
                />
                {this._displayLoading()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    textinput: {
        marginLeft: 5,
        marginRight: 5,
        height: 50,
        borderColor: '#000000',
        borderWidth: 1,
        paddingLeft: 5
    },

    view: {
        flex: 1
    },

    loading_container: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 100,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    }
})